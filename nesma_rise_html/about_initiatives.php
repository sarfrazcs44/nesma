<?php include("header.php"); ?>
<!--=====================================-->
<!--=        Newsfeed  Area Start       =-->
<!--=====================================-->
<div class="container">
    <!-- Banner Area Start -->
    <div class="banner-user">
        <div class="banner-content">
            <div class="media">
                <div class="item-img">
                    <img src="media/banner/user_1.jpg" alt="User">
                </div>
                <div class="media-body">
                    <h3 class="item-title">Initiative Title</h3>
                    <div class="item-subtitle">Initiative Category</div>
                    <ul class="user-meta">
                        <li>Type: <span>Private</span></li>
                        <li>Posts: <span>12</span></li>
                        <li>Members: <span>22</span></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="block-box user-top-header">
        <ul class="menu-list">
            <li class="active"><a href="#">About Initiative</a></li>
            <li class="ml-auto d-flex align-items-center pr-3">
                <a href="#" class="btn btn-warning text-white py-3 px-4">Join Initiative</a>
            </li>
            <li>
                <div class="dropdown">
                    <button class="dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
                        ...
                    </button>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="#">Shop</a>
                        <a class="dropdown-item" href="#">Blog</a>
                        <a class="dropdown-item" href="#">Others</a>
                    </div>
                </div>
            </li>
        </ul>
    </div>
    <div class="row">
        <div class="col-lg-8">
            <div class="block-box post-input-tab">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item flex-fill" role="presentation" data-toggle="tooltip" data-placement="top" title="STATUS">
                        <a class="nav-link active" data-toggle="tab" href="#post-status" role="tab" aria-selected="true">Notes</a>
                    </li>
                    <li class="nav-item flex-fill" role="presentation" data-toggle="tooltip" data-placement="top" title="MEDIA">
                        <a class="nav-link" data-toggle="tab" href="#post-media" role="tab" aria-selected="false">Photo/Video</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade show active" id="post-status" role="tabpanel">
                        <textarea name="status-input" id="status-input1" class="form-control textarea" placeholder="Post a textutal Note / Link inside the initiative Timeline..." cols="30" rows="6"></textarea>
                    </div>
                    <div class="tab-pane fade" id="post-media" role="tabpanel">
                        <!-- <label>Media Gallery</label>
    <a href="#" class="media-insert"><i class="icofont-upload-alt"></i>Upload</a> -->
                        <textarea name="status-input" id="status-input2" class="form-control textarea" placeholder="Post a Photo / Video inside the initiative Timeline..." cols="30" rows="6"></textarea>
                    </div>
                </div>
                <div class="post-footer">
                    <div class="submit-btn ml-auto">
                        <a href="#" class="btn-warning">Post Now</a>
                    </div>
                </div>
            </div>
            <div class="block-box post-view">
                <div class="post-header">
                    <div class="media">
                        <div class="user-img">
                            <img src="media/figure/chat_1.jpg" alt="Aahat">
                        </div>
                        <div class="media-body">
                            <div class="user-title"><a href="#">Hassan Idris</a> <i class="icofont-check"></i> uploaded <a href="#">10 new photos</a> </div>
                            <ul class="entry-meta">
                                <li class="meta-privacy"><i class="icofont-world"></i> Public</li>
                                <li class="meta-time">10 minutes ago</li>
                            </ul>
                        </div>
                    </div>
                    <div class="dropdown">
                        <button class="dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
                            ...
                        </button>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item" href="#">Close</a>
                            <a class="dropdown-item" href="#">Edit</a>
                            <a class="dropdown-item" href="#">Delete</a>
                        </div>
                    </div>
                </div>
                <div class="post-body">
                    <p>Here are some of the photos from my last visit to Cox’s Bazar</p>
                    <ul class="post-img-list">
                        <li><a href="#"><img src="media/figure/post_4.jpg" alt="Post"></a></li>
                        <li><a href="#"><img src="media/figure/post_5.jpg" alt="Post"></a></li>
                        <li><a href="#"><img src="media/figure/post_6.jpg" alt="Post"></a></li>
                        <li><a href="#"><img src="media/figure/post_7.jpg" alt="Post"></a></li>
                        <li><a href="#" data-photo="+5"><img src="media/figure/post_8.jpg" alt="Post"></a></li>
                    </ul>
                    <div class="post-meta-wrap">
                        <div class="post-meta">
                            <div class="post-reaction">
                                <div class="reaction-icon">
                                    <img src="media/figure/reaction_1.png" alt="icon">
                                    <img src="media/figure/reaction_2.png" alt="icon">
                                    <img src="media/figure/reaction_3.png" alt="icon">
                                </div>
                                <div class="meta-text">Anas Alghamdi, Mubarak and 15 others</div>
                            </div>
                        </div>
                        <div class="post-meta">
                            <div class="meta-text">2 Comments</div>
                            <div class="meta-text">05 Share</div>
                        </div>
                    </div>
                </div>
                <div class="post-footer">
                    <ul>
                        <li class="post-react">
                            <a href="#"><i class="icofont-thumbs-up"></i>React!</a>
                            <ul class="react-list">
                                <li><a href="#"><img src="media/figure/reaction_1.png" alt="Like"></a></li>
                                <li><a href="#"><img src="media/figure/reaction_2.png" alt="Like"></a></li>
                                <li><a href="#"><img src="media/figure/reaction_4.png" alt="Like"></a></li>
                                <li><a href="#"><img src="media/figure/reaction_2.png" alt="Like"></a></li>
                                <li><a href="#"><img src="media/figure/reaction_7.png" alt="Like"></a></li>
                                <li><a href="#"><img src="media/figure/reaction_6.png" alt="Like"></a></li>
                                <li><a href="#"><img src="media/figure/reaction_5.png" alt="Like"></a></li>
                            </ul>
                        </li>
                        <li><a href="#"><i class="icofont-comment"></i>Comment</a></li>
                        <li class="post-share">
                            <a href="javascript:void(0);" class="share-btn"><i class="icofont-share"></i>Share</a>
                            <ul class="share-list">
                                <li><a href="#" class="color-fb"><i class="icofont-facebook"></i></a></li>
                                <li><a href="#" class="color-messenger"><i class="icofont-facebook-messenger"></i></a></li>
                                <li><a href="#" class="color-instagram"><i class="icofont-instagram"></i></a></li>
                                <li><a href="#" class="color-whatsapp"><i class="icofont-brand-whatsapp"></i></a></li>
                                <li><a href="#" class="color-twitter"><i class="icofont-twitter"></i></a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
  
            <div class="block-box post-view">
                <div class="post-header">
                    <div class="media">
                        <div class="user-img">
                            <img src="media/figure/chat_5.jpg" alt="Aahat">
                        </div>
                        <div class="media-body">
                            <div class="user-title"><a href="user-timeline.html">Hassan Idris</a></div>
                            <ul class="entry-meta">
                                <li class="meta-privacy"><i class="icofont-user-alt-3"></i>Personal</li>
                                <li class="meta-time">8 mins ago</li>
                            </ul>
                        </div>
                    </div>
                    <div class="dropdown">
                        <button class="dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
                            ...
                        </button>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item" href="#">Close</a>
                            <a class="dropdown-item" href="#">Edit</a>
                            <a class="dropdown-item" href="#">Delete</a>
                        </div>
                    </div>
                </div>
                <div class="post-body">
                    <div class="post-no-thumbnail">
                        <p>I have great news to share with you all! I've been officially made a game streaming verified partner by Streamy http://radiustheme.com/ What does this mean? I'll be uploading new content every day, improving the quality and I'm gonna have access to games a month before the official release.</p>
                        <p>This is a dream come true, thanks to all for the support!!!</p>
                    </div>
                    <div class="post-meta-wrap">
                        <div class="post-meta">
                            <div class="post-reaction">
                                <div class="reaction-icon">
                                    <img src="media/figure/reaction_1.png" alt="icon">
                                    <img src="media/figure/reaction_2.png" alt="icon">
                                    <img src="media/figure/reaction_3.png" alt="icon">
                                </div>
                                <div class="meta-text">35</div>
                            </div>
                        </div>
                        <div class="post-meta">
                            <div class="meta-text">2 Comments</div>
                            <div class="meta-text">05 Share</div>
                        </div>
                    </div>
                </div>
                <div class="post-footer">
                    <ul>
                        <li class="post-react">
                            <a href="#"><i class="icofont-thumbs-up"></i>React!</a>
                            <ul class="react-list">
                                <li><a href="#"><img src="media/figure/reaction_1.png" alt="Like"></a></li>
                                <li><a href="#"><img src="media/figure/reaction_3.png" alt="Like"></a></li>
                                <li><a href="#"><img src="media/figure/reaction_4.png" alt="Like"></a></li>
                                <li><a href="#"><img src="media/figure/reaction_2.png" alt="Like"></a></li>
                                <li><a href="#"><img src="media/figure/reaction_7.png" alt="Like"></a></li>
                                <li><a href="#"><img src="media/figure/reaction_6.png" alt="Like"></a></li>
                                <li><a href="#"><img src="media/figure/reaction_5.png" alt="Like"></a></li>
                            </ul>
                        </li>
                        <li><a href="#"><i class="icofont-comment"></i>Comment</a></li>
                        <li class="post-share">
                            <a href="javascript:void(0);" class="share-btn"><i class="icofont-share"></i>Share</a>
                            <ul class="share-list">
                                <li><a href="#" class="color-fb"><i class="icofont-facebook"></i></a></li>
                                <li><a href="#" class="color-messenger"><i class="icofont-facebook-messenger"></i></a></li>
                                <li><a href="#" class="color-instagram"><i class="icofont-instagram"></i></a></li>
                                <li><a href="#" class="color-whatsapp"><i class="icofont-brand-whatsapp"></i></a></li>
                                <li><a href="#" class="color-twitter"><i class="icofont-twitter"></i></a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="block-box post-view">
                <div class="post-header">
                    <div class="media">
                        <div class="user-img">
                            <img src="media/figure/chat_5.jpg" alt="Aahat">
                        </div>
                        <div class="media-body">
                            <div class="user-title"><a href="user-timeline.html">Hassan Idris</a></div>
                            <ul class="entry-meta">
                                <li class="meta-privacy"><i class="icofont-user-alt-3"></i>Personal</li>
                                <li class="meta-time">8 mins ago</li>
                            </ul>
                        </div>
                    </div>
                    <div class="dropdown">
                        <button class="dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
                            ...
                        </button>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item" href="#">Close</a>
                            <a class="dropdown-item" href="#">Edit</a>
                            <a class="dropdown-item" href="#">Delete</a>
                        </div>
                    </div>
                </div>
                <div class="post-body">
                    <div class="post-no-thumbnail">
                        <p>I have great news to share with you all! I've been officially made a game streaming verified partner by Streamy http://radiustheme.com/ What does this mean? I'll be uploading new content every day, improving the quality and I'm gonna have access to games a month before the official release.</p>
                        <p>This is a dream come true, thanks to all for the support!!!</p>
                    </div>
                    <div class="post-meta-wrap">
                        <div class="post-meta">
                            <div class="post-reaction">
                                <div class="reaction-icon">
                                    <img src="media/figure/reaction_1.png" alt="icon">
                                    <img src="media/figure/reaction_2.png" alt="icon">
                                    <img src="media/figure/reaction_3.png" alt="icon">
                                </div>
                                <div class="meta-text">35</div>
                            </div>
                        </div>
                        <div class="post-meta">
                            <div class="meta-text">2 Comments</div>
                            <div class="meta-text">05 Share</div>
                        </div>
                    </div>
                </div>
                <div class="post-footer">
                    <ul>
                        <li class="post-react">
                            <a href="#"><i class="icofont-thumbs-up"></i>React!</a>
                            <ul class="react-list">
                                <li><a href="#"><img src="media/figure/reaction_1.png" alt="Like"></a></li>
                                <li><a href="#"><img src="media/figure/reaction_3.png" alt="Like"></a></li>
                                <li><a href="#"><img src="media/figure/reaction_4.png" alt="Like"></a></li>
                                <li><a href="#"><img src="media/figure/reaction_2.png" alt="Like"></a></li>
                                <li><a href="#"><img src="media/figure/reaction_7.png" alt="Like"></a></li>
                                <li><a href="#"><img src="media/figure/reaction_6.png" alt="Like"></a></li>
                                <li><a href="#"><img src="media/figure/reaction_5.png" alt="Like"></a></li>
                            </ul>
                        </li>
                        <li><a href="#"><i class="icofont-comment"></i>Comment</a></li>
                        <li class="post-share">
                            <a href="javascript:void(0);" class="share-btn"><i class="icofont-share"></i>Share</a>
                            <ul class="share-list">
                                <li><a href="#" class="color-fb"><i class="icofont-facebook"></i></a></li>
                                <li><a href="#" class="color-messenger"><i class="icofont-facebook-messenger"></i></a></li>
                                <li><a href="#" class="color-instagram"><i class="icofont-instagram"></i></a></li>
                                <li><a href="#" class="color-whatsapp"><i class="icofont-brand-whatsapp"></i></a></li>
                                <li><a href="#" class="color-twitter"><i class="icofont-twitter"></i></a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="block-box load-more-btn">
                <a href="#" class="item-btn">Load More Posts</a>
            </div>
        </div>
        <div class="col-lg-4 widget-block widget-break-lg">
            <div class="widget widget-memebers">
                <div class="widget-heading">
                    <h3 class="widget-title">Initiative President</h3>
                    <div class="dropdown">
                        <button class="dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
                            ...
                        </button>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item" href="#">Close</a>
                            <a class="dropdown-item" href="#">Edit</a>
                            <a class="dropdown-item" href="#">Delete</a>
                        </div>
                    </div>
                </div>
                <div class="members-list">
                    <div class="media">
                        <div class="item-img">
                            <a href="#">
                                <img src="media/figure/chat_1.jpg" alt="Chat">
                            </a>
                        </div>
                        <div class="media-body">
                            <h4 class="item-title"><a href="#">Hassan Idris</a></h4>
                            <div class="item-username">Nesma Holding</div>
                            <div class="member-status online d-none"><i class="icofont-check"></i></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="widget widget-gallery">
                <div class="widget-heading">
                    <h3 class="widget-title">Photo Gallery</h3>
                    <div class="dropdown">
                        <button class="dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
                            ...
                        </button>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item" href="#">Close</a>
                            <a class="dropdown-item" href="#">Edit</a>
                            <a class="dropdown-item" href="#">Delete</a>
                        </div>
                    </div>
                </div>
                <ul class="photo-list">
                    <li><a href="#"><img src="media/figure/widget_gallery1.jpg" alt="Gallery"></a></li>
                    <li><a href="#"><img src="media/figure/widget_gallery2.jpg" alt="Gallery"></a></li>
                    <li><a href="#"><img src="media/figure/widget_gallery3.jpg" alt="Gallery"></a></li>
                    <li><a href="#"><img src="media/figure/widget_gallery4.jpg" alt="Gallery"></a></li>
                    <li><a href="#"><img src="media/figure/widget_gallery5.jpg" alt="Gallery"></a></li>
                    <li><a href="#"><img src="media/figure/widget_gallery6.jpg" alt="Gallery"></a></li>
                    <li><a href="#"><img src="media/figure/widget_gallery2.jpg" alt="Gallery"></a></li>
                    <li><a href="#"><img src="media/figure/widget_gallery4.jpg" alt="Gallery"></a></li>
                    <li><a href="#" data-photo="20+"><img src="media/figure/widget_gallery1.jpg" alt="Gallery"></a></li>
                </ul>
            </div>
            <div class="widget widget-comment">
                <div class="widget-heading">
                    <h3 class="widget-title">Recent Comments</h3>
                    <div class="dropdown">
                        <button class="dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
                            ...
                        </button>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item" href="#">Close</a>
                            <a class="dropdown-item" href="#">Edit</a>
                            <a class="dropdown-item" href="#">Delete</a>
                        </div>
                    </div>
                </div>
                <div class="group-list">
                    <div class="media">
                        <div class="item-img">
                            <a href="#">
                                <img src="media/figure/widget_comment1.jpg" alt="post">
                            </a>
                        </div>
                        <div class="media-body">
                            <div class="post-date">JULY 24, 2020</div>
                            <h4 class="item-title"><a href="#">Seohen anunown printer took.</a></h4>
                        </div>
                    </div>
                    <div class="media">
                        <div class="item-img">
                            <a href="#">
                                <img src="media/figure/widget_comment2.jpg" alt="post">
                            </a>
                        </div>
                        <div class="media-body">
                            <div class="post-date">JULY 24, 2020</div>
                            <h4 class="item-title"><a href="#">Seohen anunown printer took.</a></h4>
                        </div>
                    </div>
                    <div class="media">
                        <div class="item-img">
                            <a href="#">
                                <img src="media/figure/widget_comment3.jpg" alt="post">
                            </a>
                        </div>
                        <div class="media-body">
                            <div class="post-date">JULY 24, 2020</div>
                            <h4 class="item-title"><a href="#">Seohen anunown printer took.</a></h4>
                        </div>
                    </div>
                    <div class="media">
                        <div class="item-img">
                            <a href="#">
                                <img src="media/figure/widget_comment4.jpg" alt="post">
                            </a>
                        </div>
                        <div class="media-body">
                            <div class="post-date">JULY 24, 2020</div>
                            <h4 class="item-title"><a href="#">Seohen anunown printer took.</a></h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="widget widget-groups">
                <div class="widget-heading">
                    <h3 class="widget-title">Initiative Members</h3>
                    <div class="dropdown">
                        <button class="dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
                            ...
                        </button>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item" href="#">Close</a>
                            <a class="dropdown-item" href="#">Edit</a>
                            <a class="dropdown-item" href="#">Delete</a>
                        </div>
                    </div>
                </div>
                <div class="group-list">
                    <div class="media">
                        <div class="item-img">
                            <a href="#">
                                <img src="media/groups/groups_9.jpg" alt="group">
                            </a>
                        </div>
                        <div class="media-body">
                            <h4 class="item-title"><a href="#">Hassan Idris</a></h4>
                            <div class="item-member">Nesma Holding</div>
                        </div>
                    </div>
                    <div class="media">
                        <div class="item-img">
                            <a href="#">
                                <img src="media/groups/groups_9.jpg" alt="group">
                            </a>
                        </div>
                        <div class="media-body">
                            <h4 class="item-title"><a href="#">Hassan Idris</a></h4>
                            <div class="item-member">Nesma Holding</div>
                        </div>
                    </div>
                    <div class="media">
                        <div class="item-img">
                            <a href="#">
                                <img src="media/groups/groups_9.jpg" alt="group">
                            </a>
                        </div>
                        <div class="media-body">
                            <h4 class="item-title"><a href="#">Hassan Idris</a></h4>
                            <div class="item-member">Nesma Holding</div>
                        </div>
                    </div>
                    <div class="media">
                        <div class="item-img">
                            <a href="#">
                                <img src="media/groups/groups_9.jpg" alt="group">
                            </a>
                        </div>
                        <div class="media-body">
                            <h4 class="item-title"><a href="#">Hassan Idris</a></h4>
                            <div class="item-member">Nesma Holding</div>
                        </div>
                    </div>
                </div>
                <div class="see-all-btn">
                    <a href="#" class="item-btn btn-warning">See All Members</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!--=====================================-->
<!--=        Footer Area Start       	=-->
<!--=====================================-->
<?php include("footer.php"); ?>