        <!--=====================================-->
        <!--=        Footer Area Start       	=-->
        <!--=====================================-->
        <footer class="footer-wrap footer-dashboard">
            <div class="footer-bottom">
                <div class="container">
                    <div class="footer-copyright text-left">Copyrights | Nesma Holding, 2021</div>
                </div>
            </div>
        </footer>
        </div>
        <!-- Chat Modal Here -->
        <div class="chat-conversion-box" id="chat-box-modal" tabindex="-1" aria-labelledby="chat-modal-label" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h6 class="modal-title" id="chat-modal-label">Fahim Rahman <span class="online"></span></h6>
                        <div class="action-icon">
                            <button class="chat-shrink"><i class="icofont-minus"></i></button>
                            <button type="button" class="close chat-close chat-open" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                    <div class="modal-body">
                        <ul class="chat-conversion">
                            <li class="chat-others">
                                <div class="author-img">
                                    <img src="media/figure/chat_12.jpg" alt="Chat">
                                </div>
                                <div class="author-text">
                                    <span>How are you Fahim vai ? Tommorow office will be your last day of Bachelor life.</span>
                                </div>
                            </li>
                            <li class="chat-you">
                                <div class="author-img">
                                    <img src="media/figure/chat_11.jpg" alt="Chat">
                                </div>
                                <div class="author-text">
                                    <span>hmm That's great</span>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="modal-footer">
                        <form>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Write your text here.....">
                                <div class="chat-plus-icon"><i class="icofont-plus-circle"></i></div>
                                <div class="file-attach-icon">
                                    <a href="#"><i class="icofont-slightly-smile"></i></a>
                                    <a href="#"><i class="icofont-camera"></i></a>
                                    <a href="#"><i class="icofont-image"></i></a>
                                    <a href="#"><i class="icofont-mic"></i></a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Jquery Js -->
        <script src="dependencies/jquery/js/jquery.min.js"></script>
        <script src="dependencies/popper.js/js/popper.min.js"></script>
        <script src="dependencies/bootstrap/js/bootstrap.min.js"></script>
        <script src="dependencies/imagesloaded/js/imagesloaded.pkgd.min.js"></script>
        <script src="dependencies/isotope-layout/js/isotope.pkgd.min.js"></script>
        <script src="dependencies/slick-carousel/js/slick.min.js"></script>
        <script src="dependencies/sal.js/sal.js"></script>
        <script src="dependencies/magnific-popup/js/jquery.magnific-popup.min.js"></script>
        <script src="dependencies/mcustomscrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
        <script src="dependencies/select2/js/select2.min.js"></script>
        <script src="dependencies/elevate-zoom/jquery.elevatezoom.js"></script>
        <script src="dependencies/bootstrap-validator/js/validator.min.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBtmXSwv4YmAKtcZyyad9W7D4AC08z0Rb4"></script>
        <!-- Site Scripts -->
        <script src="assets/js/app.js"></script>
    </body>

</html>