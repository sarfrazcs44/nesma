<!DOCTYPE html>
<html class="no-js" lang="">

<head>
    <!-- Meta Data -->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Cirkle | User Timeline</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="media/favicon.png">
    <link rel="stylesheet" href="dependencies/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="dependencies/icofont/icofont.min.css">
    <link rel="stylesheet" href="dependencies/slick-carousel/css/slick.css">
    <link rel="stylesheet" href="dependencies/slick-carousel/css/slick-theme.css">
    <link rel="stylesheet" href="dependencies/magnific-popup/css/magnific-popup.css">
    <link rel="stylesheet" href="dependencies/sal.js/sal.css">
    <link rel="stylesheet" href="dependencies/mcustomscrollbar/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="dependencies/select2/css/select2.min.css">

    <!-- Site Stylesheet -->
    <link rel="stylesheet" href="assets/css/app.css">
    <!-- Google Web Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:ital,wght@0,300;0,400;0,600;0,700;0,800;0,900;1,400&display=swap" rel="stylesheet">
</head>

<body class="bg-link-water">
    <!--[if lte IE 9]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
    <![endif]-->
    <a href="#wrapper" data-type="section-switch" class="scrollup">
        <i class="icofont-bubble-up"></i>
    </a>
    <!-- Preloader Start Here -->
    <div id="preloader"></div>
    <!-- Preloader End Here -->
    <div id="wrapper" class="wrapper">
        <!-- Top Header -->
        <header class="fixed-header">
            <div class="header-menu">
                <div class="navbar">
                    <div class="nav-item d-none d-sm-block">
                        <div class="header-logo">
                            <a href="home.php"><img src="media/nesmaRise.png" alt="Cirkle"></a>
                        </div>
                    </div>
                    <div class="nav-item nav-top-menu">
                        <nav id="dropdown" class="template-main-menu">
                            <ul class="menu-content">
                                <li class="header-nav-item">
                                    <a href="home.php" class="menu-link active">Home</a>
                                </li>
                                <li class="header-nav-item">
                                    <a href="index.php" class="menu-link">Initiatives</a>
                                </li>
                                <li class="header-nav-item">
                                    <a href="#" class="menu-link">Share your Idea</a>
                                </li>
                                <li class="header-nav-item">
                                    <a href="#" class="menu-link">Create an Initiative</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                    <div class="nav-item header-control">
                        <div class="inline-item d-none d-md-block">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search Initiatives">
                                <div class="input-group-append">
                                    <button class="submit-btn" type="button"><i class="icofont-search"></i></button>
                                </div>
                            </div>
                        </div>
                        <div class="inline-item d-flex align-items-center">
                            <div class="dropdown dropdown-notification">
                                <button class="dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
                                    <i class="icofont-notification"></i><span class="notify-count">3</span>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <div class="item-heading">
                                        <h6 class="heading-title">Notifications</h6>
                                        <div class="heading-btn">
                                            <a href="#">Settings</a>
                                            <a href="#">Mark all as Read</a>
                                        </div>
                                    </div>
                                    <div class="item-body">
                                        <a href="#" class="media">
                                            <div class="item-img">
                                                <img src="media/figure/notifiy_1.png" alt="Notify">
                                            </div>
                                            <div class="media-body">
                                                <h6 class="item-title">Diana Jameson</h6>
                                                <div class="item-time">15 mins ago</div>
                                                <p>when are nknowen printer took galley of types ...</p>
                                            </div>
                                        </a>
                                        <a href="#" class="media">
                                            <div class="item-img">
                                                <img src="media/figure/notifiy_2.png" alt="Notify">
                                            </div>
                                            <div class="media-body">
                                                <h6 class="item-title">Quirty</h6>
                                                <div class="item-time">15 mins ago</div>
                                                <p>when are nknowen printer took galley of types ...</p>
                                            </div>
                                        </a>
                                        <a href="#" class="media">
                                            <div class="item-img">
                                                <img src="media/figure/notifiy_3.png" alt="Notify">
                                            </div>
                                            <div class="media-body">
                                                <h6 class="item-title">Zinia Jessy</h6>
                                                <div class="item-time">15 mins ago</div>
                                                <p>when are nknowen printer took galley of types ...</p>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="item-footer">
                                        <a href="#" class="view-btn">View All Notification</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="inline-item">
                            <div class="dropdown dropdown-admin">
                                <button class="dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
                                    <span class="media">
                                        <span class="item-img">
                                            <img src="media/figure/chat_5.jpg" alt="Chat">
                                            <span class="acc-verified d-none"><i class="icofont-check"></i></span>
                                        </span>
                                        <span class="media-body text-left">
                                            <span class="item-title font-weight-bold text-dark">Abdulrhman Ali</span>
                                            <span class="item-subtitle text-secondary d-block">Committiee Member</span>
                                        </span>
                                    </span>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <ul class="admin-options">
                                        <li><a href="#">Profile Settings</a></li>
                                        <li><a href="user-groups.html">Groups</a></li>
                                        <li><a href="forums.html">Forums</a></li>
                                        <li><a href="#">Settings</a></li>
                                        <li><a href="#">Terms and Conditions</a></li>
                                        <li><a href="contact.html">Contact</a></li>
                                        <li><a href="login.html">Log Out</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <!-- Page Content -->