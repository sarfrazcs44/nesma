<?php include("header.php"); ?>
        <!-- Page Content -->
        <div class="page-content">

            <!--=====================================-->
            <!--=        Newsfeed  Area Start       =-->
            <!--=====================================-->
            <div class="container">
                <!-- Banner Area Start -->
                <div class="banner-user">
                    <div class="banner-content">
                        <div class="media">
                            <div class="item-img">
                                <img src="media/banner/user_1.jpg" alt="User">
                            </div>
                            <div class="media-body">
                                <h3 class="item-title">Rebeca Powel</h3>
                                <div class="item-subtitle">United State of America</div>
                                <ul class="item-social">
                                    <li><a href="#" class="bg-fb"><i class="icofont-facebook"></i></a></li>
                                    <li><a href="#" class="bg-twitter"><i class="icofont-twitter"></i></a></li>
                                    <li><a href="#" class="bg-dribble"><i class="icofont-dribbble"></i></a></li>
                                    <li><a href="#" class="bg-youtube"><i class="icofont-brand-youtube"></i></a></li>
                                    <li><a href="#" class="bg-behance"><i class="icofont-behance"></i></a></li>
                                </ul>
                                <ul class="user-meta">
                                    <li>Posts: <span>30</span></li>
                                    <li>Comments: <span>12</span></li>
                                    <li>Views: <span>1.2k</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block-box user-top-header">
                    <ul class="menu-list">
                        <li class="active"><a href="#">Timeline</a></li>
                        <li><a href="#">About</a></li>
                        <li><a href="#">Friends</a></li>
                        <li><a href="#">Groups</a></li>
                        <li><a href="#">Photos</a></li>
                        <li><a href="#">Videos</a></li>
                        <li><a href="#">Badges</a></li>
                        <li><a href="#">Blogs</a></li>
                        <li><a href="#">Forums</a></li>
                        <li>
                            <div class="dropdown">
                                <button class="dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
                                    ...
                                </button>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a class="dropdown-item" href="#">Shop</a>
                                    <a class="dropdown-item" href="#">Blog</a>
                                    <a class="dropdown-item" href="#">Others</a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="row">
                    <div class="col-lg-8">
                        <div class="block-box user-about">
                            <div class="widget-heading">
                                <h3 class="widget-title">Hobbies and Interests</h3>
                                <div class="dropdown">
                                    <button class="dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">...</button>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item" href="#">Close</a>
                                        <a class="dropdown-item" href="#">Edit</a>
                                        <a class="dropdown-item" href="#">Delete</a>
                                    </div>
                                </div>
                            </div>
                            <ul class="user-info">
                                <li>
                                    <label>My Hobbies:</label>
                                    <p>when an unknown printer took a galley of type and scrambledmake a type specimen It has survived not only five centuries</p>
                                </li>
                                <li>
                                    <label>Favourite Music Bands/Artists:</label>
                                    <p>Iron Maid, DC/AC, Megablow, The Ill, Kung Fighters, System of a Revenge.</p>
                                </li>
                                <li>
                                    <label>Favourite TV Shows:</label>
                                    <p>Breaking Good, RedDevil, People of Interest, The Running Dead, Found,</p>
                                </li>
                                <li>
                                    <label>Favourite Books:</label>
                                    <p>The Crime of the Century, Egiptian Mythology 101, The Scarred Wizard, Lord of the Wings, Amongst Gods, The Oracle, A Tale of Air and Water.</p>
                                </li>
                                <li>
                                    <label>Favourite Movies:</label>
                                    <p>Idiocratic, The Scarred Wizard and the Fire Crown, Crime Squad, Ferrum Man.</p>
                                </li>
                                <li>
                                    <label>Other Activities:</label>
                                    <p>Swimming, Surfing, Scuba Diving, Anime, Photography, Tattoos, Street Art.</p>
                                </li>
                            </ul>
                        </div>
                        <div class="block-box user-about">
                            <div class="widget-heading">
                                <h3 class="widget-title">Education and Employment</h3>
                                <div class="dropdown">
                                    <button class="dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">...</button>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item" href="#">Close</a>
                                        <a class="dropdown-item" href="#">Edit</a>
                                        <a class="dropdown-item" href="#">Delete</a>
                                    </div>
                                </div>
                            </div>
                            <ul class="user-info">
                                <li>
                                    <label>Education:</label>
                                    <p>The New College of Design</p>
                                </li>
                                <li>
                                    <label>Institution:</label>
                                    <p>Radiustheme.com</p>
                                </li>
                                <li>
                                    <label>Employment:</label>
                                    <p>UI/UX Designer</p>
                                </li>
                                <li>
                                    <label>Year:</label>
                                    <p>2008-2020</p>
                                </li>
                            </ul>
                        </div>
                        <div class="block-box user-about">
                            <div class="widget-heading">
                                <h3 class="widget-title">Contact Info</h3>
                                <div class="dropdown">
                                    <button class="dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">...</button>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item" href="#">Close</a>
                                        <a class="dropdown-item" href="#">Edit</a>
                                        <a class="dropdown-item" href="#">Delete</a>
                                    </div>
                                </div>
                            </div>
                            <ul class="user-info">
                                <li>
                                    <label>E-mail:</label>
                                    <p>info@crumina.net</p>
                                </li>
                                <li>
                                    <label>Phone:</label>
                                    <p>+123 98566836</p>
                                </li>
                                <li>
                                    <label>Address:</label>
                                    <p>59 Street, Newyorkc City</p>
                                </li>
                                <li>
                                    <label>Website:</label>
                                    <p><a href="#">www.rebeca.com</a></p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4 widget-block widget-break-lg">
                        <div class="widget widget-user-about">
                            <div class="widget-heading">
                                <h3 class="widget-title">About Me</h3>
                                <div class="dropdown">
                                    <button class="dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
                                        ...
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item" href="#">Close</a>
                                        <a class="dropdown-item" href="#">Edit</a>
                                        <a class="dropdown-item" href="#">Delete</a>
                                    </div>
                                </div>
                            </div>
                            <div class="user-info">
                                <p>Hi! My name is Rebeca Powel but some people may know me asserty GamePagla! I have a Newbike channel where I stream.</p>
                                <ul class="info-list">
                                    <li><span>Joined:</span>24/12/2020</li>
                                    <li><span>E-mail:</span>info@gmail.com</li>
                                    <li><span>Address:</span>59 Street Neworkcity</li>
                                    <li><span>Phone:</span>+123 9856836</li>
                                    <li><span>Country:</span>USA</li>
                                    <li><span>Web:</span><a href="#">www.rebeca.com</a></li>
                                    <li class="social-share"><span>Social:</span>
                                        <div class="social-icon">
                                            <a href="#"><i class="icofont-facebook"></i></a>
                                            <a href="#"><i class="icofont-twitter"></i></a>
                                            <a href="#"><i class="icofont-dribbble"></i></a>
                                            <a href="#"><i class="icofont-whatsapp"></i></a>
                                            <a href="#"><i class="icofont-instagram"></i></a>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="widget widget-banner">
                            <h3 class="item-title">Most Popular</h3>
                            <div class="item-subtitle">Circle Application</div>
                            <a href="#" class="item-btn">
                                <span class="btn-text">Register Now</span>
                                <span class="btn-icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="21px" height="10px">
                                        <path fill-rule="evenodd" d="M16.671,9.998 L12.997,9.998 L16.462,6.000 L5.000,6.000 L5.000,4.000 L16.462,4.000 L12.997,0.002 L16.671,0.002 L21.003,5.000 L16.671,9.998 ZM17.000,5.379 L17.328,5.000 L17.000,4.621 L17.000,5.379 ZM-0.000,4.000 L3.000,4.000 L3.000,6.000 L-0.000,6.000 L-0.000,4.000 Z" />
                                    </svg>
                                </span>
                            </a>
                            <div class="item-img">
                                <img src="media/figure/widget_banner_1.png" alt="banner">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--=====================================-->
            <!--=        Footer Area Start       	=-->
            <!--=====================================-->
            <?php include("footer.php"); ?>